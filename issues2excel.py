import gitlab
import pandas as pd


def get_project(project_list, project_id):
    for project in project_list:
        if project.attributes['id'] == project_id:
            return project
    return None


gl = gitlab.Gitlab.from_config('online', ['python-gitlab.cfg'])

for group in gl.groups.list():
    print(group.attributes["id"], group.attributes["web_url"])

my_group_id = int(input("Qual grupo?") or "6587830")
my_group = gl.groups.get(my_group_id)

first = None
for ms in my_group.milestones.list():
    if not first:
        first = ms.attributes["id"]
    print(ms.attributes["id"], ms.attributes["title"])

milestone_id = int(input(f"Qual milestone? [{first}]") or first)

milestone = my_group.milestones.get(milestone_id)

print("1. Dump issues to excel")
print("2. Remove label")
print("3. Update weights")

command = input()

if command == "1":

    issues = {
        "id": [],
        "title": [],
        "labels": [],
        "weight": [],
    }

    for issue in [x for x in my_group.issues.list() if x.attributes["milestone"] and x.attributes["milestone"]["id"] == milestone_id]:
        issues["id"].append(issue.attributes["id"])
        issues["labels"].append(', '.join(issue.attributes["labels"]))
        issues["weight"].append(issue.attributes["weight"])
        issues["title"].append(issue.attributes["title"])

    df = pd.DataFrame(issues)
    df.to_excel(open("issues.xlsx", "wb"), index=False)

elif command == "2":
    label = input("Which label?")
    for issue in [x for x in my_group.issues.list() if
                  x.attributes["milestone"] and x.attributes["milestone"]["id"] == milestone_id]:
        project = gl.projects.get(issue.project_id)
        editable_issue = project.issues.get(issue.iid)
        if label in editable_issue.labels:
            editable_issue.labels.remove(label)
            editable_issue.save()

elif command == "3":
    pass